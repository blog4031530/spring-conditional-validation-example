package com.example.conditional.validation.example;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
@Slf4j
public class ControllerAdvice {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Map<String, String>> handleValidationExceptions(MethodArgumentNotValidException e, HttpServletRequest request) {
		Map<String, String> errorDefaultMessages = new HashMap<>();
		Map<String, Object> errors = new HashMap<>();
		String objectName = e.getObjectName();
		e.getFieldErrors()
			.forEach(error -> errorDefaultMessages.put(error.getField(), error.getDefaultMessage()));

		log.error("failure check request validation, uri: {}, objectName: {}, {}", request.getRequestURI(), objectName, errors);
		return ResponseEntity.badRequest().body(errorDefaultMessages);
	}
}
