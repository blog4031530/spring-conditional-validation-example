package com.example.conditional.validation.example.jsonsubtype.dto;

import com.example.conditional.validation.example.PlayerType;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonTypeInfo(
	use = JsonTypeInfo.Id.NAME,
	property = "type",
	visible = true
)
@JsonSubTypes(
	{
		@JsonSubTypes.Type(value = HitterDTO.class, name = "HITTER"),
		@JsonSubTypes.Type(value = PitcherDTO.class, name = "PITCHER")
	}
)
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public abstract class PlayerDTO {
	@NotBlank
	private String uid;

	@NotBlank
	private String name;

	@NotNull
	private PlayerType type;
}
