package com.example.conditional.validation.example.jsonsubtype.dto;

import com.example.conditional.validation.example.PlayerType;
import jakarta.validation.constraints.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class HitterDTO extends PlayerDTO{

	//타율 (타자)
	@NotNull
	@DecimalMin("0.000") @DecimalMax("1.000")
	private BigDecimal battingAverage;

	//홈런 (타자)
	@NotNull
	@Min(0)	@Max(100)
	private Integer homeruns;

	@Builder
	public HitterDTO(String uid, String name, PlayerType type,
					 BigDecimal battingAverage, Integer homeruns) {
		super(uid, name, type);
		this.battingAverage = battingAverage;
		this.homeruns = homeruns;
	}
}
