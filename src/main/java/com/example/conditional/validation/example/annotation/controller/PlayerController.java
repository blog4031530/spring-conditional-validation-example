package com.example.conditional.validation.example.annotation.controller;

import com.example.conditional.validation.example.annotation.dto.PlayerDTO;
import com.example.conditional.validation.example.annotation.service.PlayerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/annotation")
@RequiredArgsConstructor
public class PlayerController {
	private final PlayerService playerService;

	@PostMapping(path = "/player", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PlayerDTO> createPlayer(
		@Validated
		@RequestBody PlayerDTO playerDTO) {
		PlayerDTO playerDTO1 = playerService.create(playerDTO);
		return ResponseEntity.ok(playerDTO1);
	}
}
