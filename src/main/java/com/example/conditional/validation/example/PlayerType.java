package com.example.conditional.validation.example;

import lombok.Getter;

public enum PlayerType {
	HITTER("타자"),
	PITCHER("투수"),
	;

	@Getter
	private final String role;

	PlayerType(String role) {
		this.role = role;
	}
}
