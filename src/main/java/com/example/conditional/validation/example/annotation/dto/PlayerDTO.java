package com.example.conditional.validation.example.annotation.dto;

import com.example.conditional.validation.example.PlayerType;
import com.example.conditional.validation.example.annotation.validation.Conditional;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.*;
import lombok.*;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@Conditional.List(
	{
		//type 의 값이 HITTER(타자) 인 경우 battingAverage, homeruns 값은 존재해야 함
		@Conditional(
			selected = "type",
			values = "HITTER",
			required = {"battingAverage", "homeruns"}),
		//type 의 값이 PITCHER(타자) 인 경우 era, wins 값은 존재해야 함
		@Conditional(
			selected = "type",
			values = "PITCHER",
			required = {"era", "wins"})
	}
)
public class PlayerDTO {

	@NotBlank
	private String uid;

	@NotBlank
	private String name;

	@NotNull
	private PlayerType type;

	//타율 (타자)
	@DecimalMin("0.000") @DecimalMax("1.000")
	private BigDecimal battingAverage;

	//홈런 (타자)
	@Min(0)	@Max(100)
	private Integer homeruns;

	//방어율 (투수)
	@DecimalMin("0.000") @DecimalMax("10.000")
	private BigDecimal era;

	//다승 (투수)
	@Min(0)	@Max(100)
	private Integer wins;
}
