package com.example.conditional.validation.example.jsonsubtype.dto;

import com.example.conditional.validation.example.PlayerType;
import jakarta.validation.constraints.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class PitcherDTO extends PlayerDTO{

	//방어율 (투수)
	@NotNull
	@DecimalMin("0.000") @DecimalMax("10.000")
	private BigDecimal era;

	//다승 (투수)
	@NotNull
	@Min(0)	@Max(100)
	private Integer wins;

	@Builder
	public PitcherDTO(String uid, String name, PlayerType type,
					  BigDecimal era, Integer wins) {
		super(uid, name, type);
		this.era = era;
		this.wins = wins;
	}
}
