package com.example.conditional.validation.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConditionalValidationExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConditionalValidationExampleApplication.class, args);
	}

}
