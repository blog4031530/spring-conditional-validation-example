package com.example.conditional.validation.example.annotation.service;

import com.example.conditional.validation.example.annotation.dto.PlayerDTO;
import org.springframework.stereotype.Service;

@Service
public class PlayerService {

	public PlayerDTO create(PlayerDTO playerDTO) {
		return playerDTO;
	}
}
