package com.example.conditional.validation.example.jsonsubtype.controller;

import com.example.conditional.validation.example.PlayerType;
import com.example.conditional.validation.example.jsonsubtype.dto.HitterDTO;
import com.example.conditional.validation.example.jsonsubtype.dto.PitcherDTO;
import com.example.conditional.validation.example.jsonsubtype.dto.PlayerDTO;
import com.example.conditional.validation.example.jsonsubtype.service.PlayerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = PlayerController.class)
@Import(PlayerService.class)
class PlayerControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@DisplayName("타자 등록 유효성 체크 성공 테스트")
	@Test
	void create_hitter_validation_success_test() throws Exception {
		PlayerDTO playerDTO = HitterDTO.builder()
			.uid(UUID.randomUUID().toString())
			.name("hitter1")
			.type(PlayerType.HITTER)
			.battingAverage(new BigDecimal("0.400"))
			.homeruns(50)
			.build();

		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(
				post("/jsonsubtype/player")
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					.content(objectMapper.writeValueAsString(playerDTO))
			)
			.andExpect(status().isOk())
			.andDo(print());
	}

	@DisplayName("투수 등록 유효성 체크 성공 테스트")
	@Test
	void create_pitcher_validation_success_test() throws Exception {
		PlayerDTO playerDTO = PitcherDTO.builder()
			.uid(UUID.randomUUID().toString())
			.name("pitcher")
			.type(PlayerType.PITCHER)
			.era(new BigDecimal("0.235"))
			.wins(10)
			.build();

		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(
				post("/jsonsubtype/player")
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					.content(objectMapper.writeValueAsString(playerDTO))
			)
			.andExpect(status().isOk())
			.andDo(print());
	}

	@DisplayName("타자 등록 유효성 체크 실패 테스트 - 홈런정보 누락")
	@Test
	void create_hitter_validation_fail_test() throws Exception {
		PlayerDTO playerDTO = HitterDTO.builder()
			.uid(UUID.randomUUID().toString())
			.name("hitter1")
			.type(PlayerType.HITTER)
			.battingAverage(new BigDecimal("0.400"))
			.build();

		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(
				post("/jsonsubtype/player")
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					.content(objectMapper.writeValueAsString(playerDTO))
			)
			.andExpect(status().isBadRequest())
			.andDo(print());
	}

	@DisplayName("투수 등록 유효성 체크 실패 테스트 - 다승정보 누락")
	@Test
	void create_pitcher_validation_fail_test() throws Exception {
		PlayerDTO playerDTO = PitcherDTO.builder()
			.uid(UUID.randomUUID().toString())
			.name("pitcher1")
			.type(PlayerType.PITCHER)
			.era(new BigDecimal("0.235"))
			.build();

		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(
				post("/jsonsubtype/player")
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					.content(objectMapper.writeValueAsString(playerDTO))
			)
			.andExpect(status().isBadRequest())
			.andDo(print());
	}

	@DisplayName("타자 등록 유효성 체크 실패 테스트 - 홈런 개수 limit")
	@Test
	void create_hitter_validation_limit_fail_test() throws Exception {
		PlayerDTO playerDTO = HitterDTO.builder()
			.uid(UUID.randomUUID().toString())
			.name("hitter1")
			.type(PlayerType.HITTER)
			.battingAverage(new BigDecimal("0.400"))
			.homeruns(1000)
			.build();

		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(
				post("/jsonsubtype/player")
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					.content(objectMapper.writeValueAsString(playerDTO))
			)
			.andExpect(status().isBadRequest())
			.andDo(print());
	}

	@DisplayName("투수 등록 유효성 체크 실패 테스트 - 다승 개수 limit")
	@Test
	void create_pitcher_validation_limit_fail_test() throws Exception {
		PlayerDTO playerDTO = PitcherDTO.builder()
			.uid(UUID.randomUUID().toString())
			.name("pitcher1")
			.type(PlayerType.PITCHER)
			.era(new BigDecimal("0.235"))
			.wins(1000)
			.build();

		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(
				post("/jsonsubtype/player")
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					.content(objectMapper.writeValueAsString(playerDTO))
			)
			.andExpect(status().isBadRequest())
			.andDo(print());
	}

	@DisplayName("타자 생성시 투수 정보도 입력했을 때 테스트")
	@Test
	void create_player_include_all_fields_test() throws Exception {
		com.example.conditional.validation.example.annotation.dto.PlayerDTO playerDTO =
			com.example.conditional.validation.example.annotation.dto.PlayerDTO
				.builder()
				.uid(UUID.randomUUID().toString())
				.name("hitter1")
				.type(PlayerType.HITTER)
				.battingAverage(new BigDecimal("0.300"))
				.homeruns(50)
				.era(new BigDecimal("0.000"))
				.wins(20)
				.build();

		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(
				post("/jsonsubtype/player")
					.contentType(MediaType.APPLICATION_JSON_VALUE)
					.content(objectMapper.writeValueAsString(playerDTO))
			)
			.andExpect(status().isOk())
			.andDo(print());
	}
}